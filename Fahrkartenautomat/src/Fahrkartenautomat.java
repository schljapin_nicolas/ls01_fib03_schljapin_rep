﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	Scanner tastatur = new Scanner(System.in);
       double zuZahlenderBetrag;
       int anzahlDerFahrkarten;
       double eingezahlterGesamtbetrag;
       double rückgabebetrag = 0.0;
       String s = "Noch zu zahlen: ";
       
       while(true) 
       {
    	   System.out.println("Um die Anwendung zu beenden drücken Sie 'E', ansonsten eine beliebige Taste.");
    	   String schließen = tastatur.next();
    	   if(schließen.equalsIgnoreCase("E")) 
    	   {
    		   System.exit(0);
    		   break;
    	   }
    	   
    	   zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	   if (zuZahlenderBetrag == 0.00) 
    	   {
    	 		 System.out.println(" >>falsche Eingabe<<");
    	 		 System.out.println("System wird geschlossen.");
    	 		 return;
    	   }
    	   System.out.println("Anzahl der Tickets: ");
    	   anzahlDerFahrkarten = fahrkartenAnzahlErfassen();
    	   if (anzahlDerFahrkarten <= 0)
    	   {
    		   System.out.println("Bitte einen gültigen Wert eingeben (Hinweis: nur Ganzzahlen über Null werden erkannt).");
    		   System.out.println("System wird geschlossen.");
    		   return;
    	   }
    	   // Geldeinwurf
    	   // -----------
    	   eingezahlterGesamtbetrag = 0.0;
    	   eingezahlterGesamtbetrag = fahrkartenBezahlen(s, eingezahlterGesamtbetrag, zuZahlenderBetrag, anzahlDerFahrkarten);
    	   
    	   // Fahrscheinausgabe
    	   // -----------------
    	   fahrkartenAusgeben();
    	   
    	   // Warte Methode zusätzlich, Ich wollte nicht die Methode "fahrkartenAusgeben" überarbeiten und erstelle daher hier die Mehtode
    	   warte(500);
    	   // Rückgeldberechnung und -Ausgabe
    	   // -------------------------------
    	   rueckgeldAusgeben(rückgabebetrag, eingezahlterGesamtbetrag, zuZahlenderBetrag);
    	   
    	   System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
    			   "vor Fahrtantritt entwerten zu lassen!\n"+
    			   "Wir wünschen Ihnen eine gute Fahrt.");
       }
       
       }

    /* 1. Arrays können schnell deklariert und verändert werden. Außerdem kann man mit ihnen mit wenig Code Listen erzeugen und durch den Index jedes Objekt erreichen.
     * 3. Wie vorher erwähnt ist die Änderung (Einführung neuer oder Löschen momentaner) Objekte recht simpel. Allerdings werden nur einzelne Information pro Array gespeichert.
     *  Dies sind außerdem keine Felder und ein Array hat immer einen Datentyp. Dazu können Arrays (wenn man sie nicht manuell begrenzt was auch zu Problemen führen kann) sehr 
     *  Speicherineffizient werden, wenn sie eine gewisse größe erreichen. 
     */
    public static double fahrkartenbestellungErfassen()
    {
    	double[] fahrkartenpreise = { 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
    	String[] fahrkartenbezeichnung = { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein ABC", "Kurzstrecke", "Tageskarte Berlin AB",
    			"Tageskarte BC", "Tageskarte ABC", "Kleingruppen-Tageskarte AB", "Kleingruppen-Tageskarte BC", "Kleingruppen-Tageskarte ABC"}; 
    	double kosten = 0.00;
    	int tarif;    		
    	do 
    	{
    		for (int i = 0; i < fahrkartenbezeichnung.length; i++) 
    		{
    			System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin aus:" + "\n");
    			System.out.println(fahrkartenbezeichnung[i] + " " + fahrkartenpreise[i] + "\n");
    			
    		}
    		System.out.print("Ihre Wahl: ");
    		tarif = new Scanner(System.in).nextInt();
    		
    		switch(tarif) 
    		{
    			case 0:
    				kosten += fahrkartenpreise[tarif];
    				break;
    		}
    	}
	    	while (tarif != 10);
    return kosten;
    }
    
    public static int fahrkartenAnzahlErfassen() 
    {

    	return new Scanner(System.in).nextInt();
    }
    
    public static double fahrkartenBezahlen(String s,double eingezahlterGesamtbetrag, double zuZahlenderBetrag, int anzahlDerFahrkarten) 
    {
    	Scanner tastatur = new Scanner(System.in);
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag * anzahlDerFahrkarten)
        {
     	   System.out.printf("%s %.2f€" + "%n", s,(zuZahlenderBetrag * anzahlDerFahrkarten - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	  double eingeworfeneMünze = tastatur.nextDouble();
          eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	 
    	return eingezahlterGesamtbetrag;
    }
    
    public static void fahrkartenAusgeben() 
    {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    public static void warte(int millisekunde) 
    {
    	try {
 			Thread.sleep(millisekunde);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}	
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag, double eingezahlterGesamtbetrag, double zuZahlenderBetrag) 
    {
    	String einheit = new String();
    	int betrag = 0;
    	rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rückgabebetrag > 0.00)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
            	betrag = 2;
            	einheit = " Euro";
         	  muenzenAusgeben(betrag, einheit);
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
            	betrag = 1;
            	einheit = " Euro";
            	muenzenAusgeben(betrag, einheit);
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
            	betrag = 50;
            	einheit = " Cent";
            	muenzenAusgeben(betrag, einheit);
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
            	betrag = 20;
            	einheit = " Cent";
            	muenzenAusgeben(betrag, einheit);
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
            	betrag = 10;
            	einheit = " Cent";
            	muenzenAusgeben(betrag, einheit);
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
            	betrag = 5;
            	einheit = " Cent";
            	muenzenAusgeben(betrag, einheit);
  	          rückgabebetrag -= 0.05;
            }
            while(rückgabebetrag > 0) 
            {
            	betrag = 1;
            	einheit = " Cent";
            	muenzenAusgeben(betrag, einheit);
         	   rückgabebetrag -= 0.01;
            }
        }   
    }
    
    public static void muenzenAusgeben(int betrag, String einheit)
    {
    	System.out.println(betrag + einheit);
    }
}